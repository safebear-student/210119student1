package com.safebear.auto.nonBddTests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

        @Test
        public void validLogin() {
            //1. Action go to login page
            driver.get(Utils.getUrl());
            //1. Expected check I'm on login page
            Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
            //2. Action login as a valid user
            loginPage.login("tester","letmein");
            //2. Expected Check I'm on tools page
            Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page");
            //3. Expected Check success message is shown
            Assert.assertTrue(toolsPage.checkForSuccessfulLoginMessage().contains("Success"));
        }

        @Test
        public void invalidLogin() {
            //1. Action go to login page
            driver.get(Utils.getUrl());
            //1. Expected check I'm on login page
            Assert.assertEquals(loginPage.getPageTitle(),loginPage.getExpectedPageTitle());
            //2. Action login as an invalid user
            loginPage.login("hacker","letmein");
            //2. Expected Check I'm still on login page
            Assert.assertEquals(toolsPage.getPageTitle(),"Login Page");
            //3. Expected Check success message is shown
            Assert.assertTrue(loginPage.checkForFailedLoginMessageWarning().contains("incorrect"));
        }
    }
