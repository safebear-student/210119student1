package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators locators = new LoginPageLocators();
    @NonNull WebDriver driver;
    String expectedPageTitle = "Login Page";

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String getExpectedPageTitle()  {
        return expectedPageTitle;
    }

    public void enterUsername (String username){

        driver.findElement(locators.getUsernameLocator()).sendKeys(username);
    }
    public void enterPasswWord (String password){

        driver.findElement(locators.getPasswordLocator()).sendKeys(password);
    }

    public void clickLoginButton(){

        driver.findElement(locators.getLoginButtonLocator()).click();

    }
    public String checkForFailedLoginMessageWarning()
    {
        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }

    public void login(String Username, String Password){

        enterUsername(Username);
        enterPasswWord(Password);
        clickLoginButton();
    }
}


