package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest{

@Test
public void employeeTest(){
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

        hannah.employ();
        bob.fire();

        victoria.employ();
        victoria.changeCar("bmw");

        System.out.println("Hannah employment state: "+ hannah.isEmployed());
        System.out.println("Bob employment state: "+ bob.isEmployed());
        System.out.println("Victoria employment state: " + victoria.isEmployed());
        System.out.println("Victoria's car:" + victoria.car);

        } }
