package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.eclipse.jdt.internal.compiler.flow.UnconditionalFlowInfo;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

public class StepDefs {

    WebDriver driver;

    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Utils.getDriver();

        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown(){
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep","2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }


    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "We're not on the login page or the page title has changed");

    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_User(String userType) throws Throwable {

        switch (userType) {
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPasswWord("letmein");
                loginPage.clickLoginButton();
                break;

            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPasswWord("letmein");
                loginPage.clickLoginButton();
                break;

            default:
                Assert.fail("The test data is wrong - the only values that can be accepted are 'validUser' or 'invalidUser'");
                break;



        }
    }

    @Then("^I can see the following message '(.+)'" +
            "$")
    public void i_can_see_the_following_message(String validationMessage) throws Throwable {

        switch(validationMessage)
        {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginPage.checkForFailedLoginMessageWarning().contains(validationMessage));
        break;

            case "Login Successful":
                Assert.assertTrue(toolsPage.checkForSuccessfulLoginMessage().contains(validationMessage));
                break;
                default:
                    Assert.fail("The test data is wrong");
                break;



    }


}}

