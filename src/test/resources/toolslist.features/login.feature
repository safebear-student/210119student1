Feature: Login

  In order to access the website
  As a user
  I want to know if my login is successful

  Rules
  *The User must be informed if the login information is incorrect
  *The User must be informed if the login is successful

  Glossary
  *User

  @HighRisk
  @Regression

    Scenario Outline: Navigate and login to the application
    Given I navigate to the login page
    When I enter the login details for a '<userType>'
    Then I can see the following message '<validationMessage>'
    Examples:
    | userType | validationMessage |
    |invalidUser | Username or Password is incorrect |
    |validUser | Login Successful |

